bin/testground.out fullinfo "1 exp_BI" && python3 script/write_patches.py
bin/testground.out fullinfo "2 bJ_sin" && python3 script/write_patches.py
bin/testground.out fullinfo "3 di_tan" && python3 script/write_patches.py
bin/testground.out fullinfo "4 log_erf" && python3 script/write_patches.py
bin/testground.out fullinfo "5 acos_fd" && python3 script/write_patches.py
bin/testground.out fullinfo "6 ei" && python3 script/write_patches.py
bin/testground.out fullinfo "7 Q1_W" && python3 script/write_patches.py
bin/testground.out fullinfo "8 bj_tan" && python3 script/write_patches.py
bin/testground.out fullinfo "9 Si_tan" && python3 script/write_patches.py
bin/testground.out fullinfo "10 by_psi" && python3 script/write_patches.py
bin/testground.out fullinfo "11 fdm_log" && python3 script/write_patches.py
bin/testground.out fullinfo "12 eQ_sqrt" && python3 script/write_patches.py
bin/testground.out fullinfo "13 W_var" && python3 script/write_patches.py
bin/testground.out fullinfo "14 W_log" && python3 script/write_patches.py
bin/testground.out fullinfo "15 pow_df" && python3 script/write_patches.py
bin/testground.out fullinfo "16 chi_ci" && python3 script/write_patches.py
bin/testground.out fullinfo "17 fc_bj" && python3 script/write_patches.py
bin/testground.out fullinfo "s1 cos_x2" && python3 script/write_patches.py
bin/testground.out fullinfo "s2 exp_2" && python3 script/write_patches.py
bin/testground.out fullinfo "s3 cos_sin" && python3 script/write_patches.py
bin/testground.out fullinfo "s4 sin_sin" && python3 script/write_patches.py
bin/testground.out fullinfo "s5 tan_tan" && python3 script/write_patches.py
bin/testground.out fullinfo "s6 cos_cos" && python3 script/write_patches.py
bin/testground.out fullinfo "s7 exp_exp" && python3 script/write_patches.py
bin/testground.out fullinfo "s8 exp_1" && python3 script/write_patches.py
bin/testground.out fullinfo "s9 x_tan" && python3 script/write_patches.py
bin/testground.out fullinfo "s10 log_log" && python3 script/write_patches.py
bin/testground.out fullinfo "s11 log_x" && python3 script/write_patches.py
bin/testground.out fullinfo "s12 sqrt_exp" && python3 script/write_patches.py
bin/testground.out fullinfo "s13 sin_tan" && python3 script/write_patches.py
bin/testground.out fullinfo "s14 exp_x" && python3 script/write_patches.py
bin/testground.out fullinfo "s15 x_x2" && python3 script/write_patches.py
bin/testground.out fullinfo "c1 control_1" && python3 script/write_patches.py
bin/testground.out fullinfo "c2 control_2" && python3 script/write_patches.py
bin/testground.out fullinfo "c3 control_3" && python3 script/write_patches.py
bin/testground.out fullinfo "c4 control_4" && python3 script/write_patches.py
