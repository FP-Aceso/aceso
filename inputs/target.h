#ifndef TARGETS_H
#define TARGETS_H

#include <functional>
#include <vector>

// targets.c
extern "C" double exp_BI(double);
extern "C" double bJ_sin(double);
extern "C" double di_tan(double);
extern "C" double log_erf(double);
extern "C" double acos_fd(double);
extern "C" double ei(double);
extern "C" double Q1_W(double);
extern "C" double bj_tan(double);
extern "C" double Si_tan(double);
extern "C" double by_psi(double);
extern "C" double fdm_log(double);
extern "C" double eQ_sqrt(double);
extern "C" double W_var(double);
extern "C" double W_log(double);
extern "C" double cos_x2(double);
extern "C" double exp_2(double);
extern "C" double cos_sin(double);
extern "C" double sin_sin(double);
extern "C" double tan_tan(double);
extern "C" double cos_cos(double);
extern "C" double exp_exp(double);
extern "C" double exp_1(double);
extern "C" double x_tan(double);
extern "C" double log_log(double);
extern "C" double log_x(double);
extern "C" double sqrt_exp(double);
extern "C" double sin_tan(double);
extern "C" double exp_x(double);
extern "C" double x_x2(double);
extern "C" double control_1(double);
extern "C" double control_2(double);
extern "C" double control_3(double);
extern "C" double control_4(double);
extern "C" double pow_df(double);
extern "C" double chi_ci(double);
extern "C" double fc_bj(double);
extern "C" double gb_sqrt(double);
extern "C" double l1_l2(double);
extern "C" double hyp_g2(double);

const std::vector<std::function<double(double)>> simpleFuncList = {
    exp_BI,
    bJ_sin,
    di_tan,
    log_erf,
    acos_fd,
    ei,
    Q1_W,
    bj_tan,
    Si_tan,
    by_psi,
    fdm_log,
    eQ_sqrt,
    W_var,
    W_log,
    pow_df,
    chi_ci,
    fc_bj,
    cos_x2,
    exp_2,
    cos_sin,
    sin_sin,
    tan_tan,
    cos_cos,
    exp_exp,
    exp_1,
    x_tan,
    log_log,
    log_x,
    sqrt_exp,
    sin_tan,
    exp_x,
    x_x2,
    control_1,
    control_2,
    control_3,
    control_4,
};

#endif