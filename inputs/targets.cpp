#include <math.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_cdf.h>
#include "target.h"
#include <libalglib/specialfunctions.h>

double cos_x2(double x) {
    double val = (1 - cos(x)) / (x*x);
    return val;
}

double exp_2(double x) {
    double val = ((exp(x) - 2) + exp(-x))/x;
    return val;
}

double cos_sin(double x) {
    double val = (1 - cos(x)) / sin(x);
    return val;
}

double sin_sin(double x) {
    double eps = 1e-6;
    double val = sin(x + eps) - sin(x);
    return val;
}

double tan_tan(double x) {
    double eps = 1e-6;
    double val = tan(x + eps) - tan(x);
    return val;
}

double cos_cos(double x) {
    double eps = 1e-6;
    double val = cos(x + eps) - cos(x);
    return val;
}

double exp_exp(double x) {
    double val = exp(x) - exp(-x);
    return val;
}

double exp_1(double x) {
    double val = exp(x) - 1;
    return val;
}

double x_tan(double x) {
    double val = 1 / x - 1 / tan(x);
    return val;
}

double log_log(double x) {
    double val = log(1 - x) / log(1 + x);
    return val;
}

double log_x(double x) {
    double val = log((1 - x) / (1 + x));
    return val;
}

double sqrt_exp(double x) {
    double val = sqrt((exp(2 * x) - 1) / (exp(x) - 1));
    return val;
}

double sin_tan(double x) {
    double val = (x - sin(x)) / (x - tan(x));
    return val;
}

double exp_x(double x) {
    double val = ((exp(x) - 1) / x);
    return val;
}

double x_x2(double x) {
    double val = ((x - 1) / ((x * x) - 1));
    return val;
}

double exp_BI(double x) {
    double bx = gsl_sf_bessel_I1(x);
    double val = ((exp(bx) - 1.0) / x);
    return val;
}

double bJ_sin(double x) {
    double bx = gsl_sf_bessel_J0(x);
    double val = (1 - bx) / sin(x);
    return val;
}

double di_tan(double x) {
    double dx = gsl_sf_dilog(x);
    double val = 1/dx - 1/tan(x);
    return val;
}

double log_erf(double x) {
    double ex = gsl_sf_erf(x);
    double val = log(1-ex) / log(1+x);
    return val;
}

double acos_fd(double x) {
    double fx = gsl_sf_fermi_dirac_1(x);
    double val = (acos(x)*acos(x) - 3*fx) / x;
    return val;
}

double control_1(double x) {
    double val = exp(sin(x) * cos(x));
    return val;
}

double control_2(double x) {
    double val = sqrt(exp(x+1));
    return val;
}

double control_3(double x) {
    double val = sin(2*x) / sin(x);
    return val;
}

double control_4(double x) {
    double bx = gsl_sf_bessel_J0(x);
    double val = sqrt(sin(x)*sin(x) + bx*bx);
    return val;
}

double ei(double x) {
    // double val = sin(boost::math::erf_inv(x))/(cos(x)-exp(x));
    double eix = gsl_cdf_ugaussian_Pinv(x/2.0 + 0.5) / sqrt(2.0);
    double val = sin(eix) / (cos(x) - exp(x));
    return val;
}

double Q1_W(double x) {
    double lx = gsl_sf_lambert_W0(x);
    double val = (1+gsl_sf_legendre_Q1(x))/(lx*lx);
    return val;
}

double bj_tan(double x) {
    double jx = gsl_sf_bessel_j0(x);
    double val = (1-jx)/(x*tan(x));
    return val;
}

double Si_tan(double x) {
    double sx = gsl_sf_Si(x);
    double val = (sx - tan(x)) / (x * x * x);
    return val;
}

double by_psi(double x) {
    double yx = (x >= 0) ? gsl_sf_bessel_y0(x) : -gsl_sf_bessel_y0(-x);
    double tx = gsl_sf_psi_1(x);
    double val = yx * yx - tx;
    return val;
}

double fdm_log(double x) {
    double fdx = gsl_sf_fermi_dirac_m1(x);
    double val = (2*fdx-1)/log(1+x);
    return val;
}

double eQ_sqrt(double x) {
    double qx = gsl_sf_erf_Q(x);
    double val = (2 * qx - sqrt(1 + x)) / x;
    return val;
}

double W_var(double x) {
    double lx = gsl_sf_lambert_W0(x);
    return (lx-1) / (lx*lx - 1);
}

double W_log(double x) {
    double lx = gsl_sf_lambert_W0(x);
    return (lx-1) / (lx*log(x) - 1);
}

double pow_df(double x) {
    double val = pow(1+alglib::dawsonintegral(x), 1.0/x);
    return val;
}

double chi_ci(double x) {
    double chix, shix;
    alglib::hyperbolicsinecosineintegrals(x, shix, chix);
    double cix, six;
    alglib::sinecosineintegrals(x, six, cix);
    double val = (chix - cix)/(x*x);
    return val;
}

double fc_bj(double x) {
    double sx, cx;
    alglib::fresnelintegral(x, cx, sx);
    double val = 1.0/cx + gsl_sf_bessel_j1(x) - sin(x)/(x*x);
    return val;
}

double gb_sqrt(double x) {
    double v1 = gsl_sf_gegenpoly_1(0.5, x);
    double v2 = gsl_sf_gegenpoly_n(0, 1, x);
    double val = (sqrt(v1 + 1) - 2) / (x - 3*v2);
    return val;
}

double l1_l2(double x) {
    double l1x = gsl_sf_laguerre_1(0, x);
    double l2x = gsl_sf_laguerre_2(-1, x);
    double val = (log(x)+l1x) / (l2x+0.5);
    return val;
}

double hyp_g2(double x) {
    double hx = gsl_sf_hypot(x, 1.0/x);
    double gx = gsl_sf_gegenpoly_2(1, x);
    double val = (sqrt(2) * hx - 2.0) / (gx + 5.0 - 8.0*x);
    return val;
}