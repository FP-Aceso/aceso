#include "gsl-fit.h"
#include <cstdio>
#include "../inputs/oracles.h"
#include <chrono>
#include <random>
#include "serializer.h"
#include <array>

#define FullInfoLevel 4
#define NoInfoLevel 0

#define USE_WEIGHT 1
#define NO_WEIGHT  0

using namespace std;

std::vector<double> randomOnValue(double l, double r, int nums) {
    std::vector<double> xs;
    std::random_device rd;  // Will be used to obtain a seed for the random number engine
    std::mt19937_64 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(l, r);

    for (int i = 0; i < nums; i++) {
        xs.push_back(dis(gen));
    }
    return xs;
}

std::vector<double> randomOnMagnitude(double lowMag, double HighMag, int nums) {
    std::vector<double> xs;
    std::random_device rd;  // Will be used to obtain a seed for the random number engine
    std::mt19937_64 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(lowMag, HighMag);

    for (int i = 0; i < nums; i++) {
        double x = exp10(dis(gen));
        xs.push_back(x);
    }
    return xs;
}

const std::vector<std::string> functionIDs = {
    "1 exp_BI",
    "2 bJ_sin",
    "3 di_tan",
    "4 log_erf",
    "5 acos_fd",
    "6 ei",
    "7 Q1_W",
    "8 bj_tan",
    "9 Si_tan",
    "10 by_psi",
    "11 fdm_log",
    "12 eQ_sqrt",
    "13 W_var",
    "14 W_log",
    "15 pow_df",
    "16 chi_ci",
    "17 fc_bj",
    "s1 cos_x2",
    "s2 exp_2",
    "s3 cos_sin",
    "s4 sin_sin",
    "s5 tan_tan",
    "s6 cos_cos",
    "s7 exp_exp",
    "s8 exp_1",
    "s9 x_tan",
    "s10 log_log",
    "s11 log_x",
    "s12 sqrt_exp",
    "s13 sin_tan",
    "s14 exp_x",
    "s15 x_x2",
    "c1 control_1",
    "c2 control_2",
    "c3 control_3",
    "c4 control_4",
};

const std::vector<std::pair<double, double>> errIntervals = {
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {2.718281828459045-1e-2, 2.718281828459045+1e-2}, // W_var
    {2.718281828459045-1e-2, 2.718281828459045+1e-2}, // W_log
    {-1e-2, 1e-2}, // 15
    {-1e-2, 1e-2}, // 16
    {-1e-2, 1e-2}, // 17
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {1.5607963267948966192313216916398, 1.5807963267948966192313216916398},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {1-1e-2, 1+1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
    {-1e-2, 1e-2},
};

const int defaultRootsNum = 1000;
const int defaultDegree   = 6;
const int defaultPoly     = MODE_CHEBY;

const int evaluationNum  = 5000;

int measurePatchRuntime(int funcIndex, int weighted) {
    Polynomial poly;
    double lbound = errIntervals[funcIndex].first;
    double rbound = errIntervals[funcIndex].second;
    int rootsNum  = defaultRootsNum;
    int highestDegree = defaultDegree;
    int polymode = defaultPoly;
    Observation observation(funcIndex, lbound, rbound, rootsNum);
    observation.init();

    GSLFitModule gslfit;
    gslfit.init(highestDegree, weighted, polymode, lbound, rbound);
    gslfit.fitObservations(observation);
    poly = gslfit.getPoly();
    poly.checkacc0();

    // prepare inputs
    int EvalTimeNum = 1000000;
    auto inputsvec = randomOnValue(lbound, rbound, EvalTimeNum);
    double inputs[1000000];
    for (int i = 0; i < EvalTimeNum; i++) {
        inputs[i] = inputsvec[i];
    }

    auto start_time = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < EvalTimeNum; i++) {
        poly.evaluate(inputs[i]);
    }
    auto end_time = std::chrono::high_resolution_clock::now();
    int64_t time_on_patch = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();

    auto func = simpleFuncList[funcIndex];
    start_time = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < EvalTimeNum; i++) {
        func(inputs[i]);
    }
    end_time = std::chrono::high_resolution_clock::now();
    int64_t time_on_origin = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();

    printf("Function and Time: %d, %ld, %ld\n", funcIndex, time_on_patch, time_on_origin);
    return 0;
}

int fitFullInfo(int funcIndex, int weighted) {
    Serializer ser;
    ser.writeFuncIDToFile(functionIDs[funcIndex]);

    /////////////////// Synthesizing ///////////////////
    printf("Synthesizing Patch...\n");
    // measure time
    auto start_time = std::chrono::high_resolution_clock::now();

    Polynomial poly;
    double lbound = errIntervals[funcIndex].first;
    double rbound = errIntervals[funcIndex].second;
    int rootsNum  = defaultRootsNum;
    int highestDegree = defaultDegree;
    int polymode = defaultPoly;
    Observation observation(funcIndex, lbound, rbound, rootsNum);
    observation.init();

    GSLFitModule gslfit;
    gslfit.init(highestDegree, weighted, polymode, lbound, rbound);
    gslfit.fitObservations(observation);
    poly = gslfit.getPoly();
    poly.checkacc0();

    auto end_time = std::chrono::high_resolution_clock::now();

    printf("Synthesized Polynomial: \n");
    poly.show();
    poly.writePolyToFile();
    int64_t time_on_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
    printf("Time on Synthesizing: %ld ms\n\n", time_on_ms);

    /////////////////// Validation ///////////////////

    printf("Validating... ");
    start_time = std::chrono::high_resolution_clock::now();

    bool valid = true;
    int score = observation.evaluatePoly(poly);
    if (score > 0) {
        printf("Valid.\n");
        valid = true;
    }
    else {
        printf("Invalid.\n");
        valid = false;
    }
    ser.writeValidationToFile(valid);

    end_time = std::chrono::high_resolution_clock::now();

    time_on_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
    printf("Time on Validation: %ld ms\n", time_on_ms);
    if (valid == false) {
        printf("[*DENIED Patch*] during validation on FuncIndex: %d\n", funcIndex);
    }

    /////////////////// Evaluation with Oracle ///////////////////

    printf("\nEvaluation with Oracles...\n");
    auto originFunc = simpleFuncList[funcIndex];
    auto oracleFunc = oracleFuncList[funcIndex];

    // On accurate area
    double stable_origin_max_relative_err = 0;
    double stable_patch_max_relative_err = 0;

    double stable_origin_max_absolute_err = 0;
    double stable_patch_max_absolute_err = 0;

    auto inputs = randomOnValue(lbound, rbound, evaluationNum);
    for (int i = 0; i < evaluationNum; i++) {
        double xi = inputs[i];
        double origin_res = originFunc(xi);
        double patch_res = poly.evaluate(xi);
        mpreal oracle_res = oracleFunc(xi);

        double origin_absolute_err = (double)fabs(origin_res - oracle_res);
        double patch_absolute_err  = (double)fabs(patch_res - oracle_res);
        double origin_relative_err = (double)fabs((origin_res - oracle_res) / oracle_res);
        double patch_relative_err  = (double)fabs((patch_res - oracle_res) / oracle_res);

        stable_origin_max_relative_err = max(stable_origin_max_relative_err, origin_relative_err);
        stable_patch_max_relative_err  = max(stable_patch_max_relative_err, patch_relative_err);
        stable_origin_max_absolute_err = max(stable_origin_max_absolute_err, origin_absolute_err);
        stable_patch_max_absolute_err  = max(stable_patch_max_absolute_err, patch_absolute_err);
    }
    printf("Stable Area Improved By Aceso (abs): %.2e -> %.2e\n", stable_origin_max_absolute_err, stable_patch_max_absolute_err);
    printf("Stable Area Improved By Aceso (rel): %.2e -> %.2e\n", stable_origin_max_relative_err, stable_patch_max_relative_err);

    // On decayed area
    double decayed_origin_max_relative_err = 0;
    double decayed_patch_max_relative_err = 0;

    double decayed_origin_max_absolute_err = 0;
    double decayed_patch_max_absolute_err = 0;

    auto mags = randomOnMagnitude(-20, -2, evaluationNum);
    for (int i = 0; i < evaluationNum; i++) {
        double xi = (lbound + rbound)/2 + ((i%2==0) ? mags[i] : -mags[i]);
        double origin_res = originFunc(xi);
        double patch_res = poly.evaluate(xi);
        mpreal oracle_res = oracleFunc(xi);

        double origin_absolute_err = (double)fabs(origin_res - oracle_res);
        double patch_absolute_err  = (double)fabs(patch_res - oracle_res);
        double origin_relative_err = (double)fabs((origin_res - oracle_res) / oracle_res);
        double patch_relative_err  = (double)fabs((patch_res - oracle_res) / oracle_res);

        decayed_origin_max_relative_err = max(decayed_origin_max_relative_err, origin_relative_err);
        decayed_patch_max_relative_err  = max(decayed_patch_max_relative_err, patch_relative_err);
        decayed_origin_max_absolute_err = max(decayed_origin_max_absolute_err, origin_absolute_err);
        decayed_patch_max_absolute_err  = max(decayed_patch_max_absolute_err, patch_absolute_err);
    }
    printf("Decayed Area Improved By Aceso (abs): %.2e -> %.2e\n", decayed_origin_max_absolute_err, decayed_patch_max_absolute_err);
    printf("Decayed Area Improved By Aceso (rel): %.2e -> %.2e\n", decayed_origin_max_relative_err, decayed_patch_max_relative_err);

    printf("\nEvaluation on Function: %s Done.\n", functionIDs[funcIndex].c_str());

    printf("==============\n");

    ser.writeEvaluationToFile(
        decayed_origin_max_relative_err,
        decayed_patch_max_relative_err,
        decayed_origin_max_absolute_err,
        decayed_patch_max_absolute_err,
        stable_origin_max_relative_err,
        stable_patch_max_relative_err,
        stable_origin_max_absolute_err,
        stable_patch_max_absolute_err
    );
    return 0;
}

int fitFPResult(int funcIndex, int weighted) {
    auto originFunc = simpleFuncList[funcIndex];
    auto oracleFunc = oracleFuncList[funcIndex];

    Serializer ser;
    ser.writeFuncIDToFile(functionIDs[funcIndex]);
    // No Microstructure, cannot validate.
    ser.writeValidationToFile(true);

    /////////////////// Synthesizing ///////////////////
    Polynomial poly;
    double lbound = errIntervals[funcIndex].first;
    double rbound = errIntervals[funcIndex].second;
    int num  = defaultRootsNum;
    int highestDegree = defaultDegree;
    int polymode = defaultPoly;
    // Observation observation(funcIndex, lbound, rbound, num);
    // observation.init();

    GSLFitModule gslfit;
    // Fit with direct FP values
    gslfit.init(highestDegree, weighted, polymode, lbound, rbound);
    std::vector<double> xs;
    for (int k = 0; k <= num; k++) {
        double fpk = k;
        double fpn = num+1;
        double rt = cosl((2*fpk+1)/(2*fpn)*myPi);
        rt = (lbound+rbound)/2 + rt*(rbound-lbound)/2;
        xs.push_back(rt);
    }
    std::vector<double> ys;
    std::vector<double> rxs;
    poly.lbound = lbound;
    poly.rbound = rbound;
    for (double xi : xs) {
        double y = originFunc(xi);
        if (isnan(y) || isinf(y))
            continue;
        else {
            ys.push_back(originFunc(xi));
            rxs.push_back(poly.reduction(xi));
        }
    }

    std::vector<double> coef = gslfit._fitCenterPointsOnly(rxs, ys);
    poly.coef = coef;
    poly.mode = defaultPoly;

    printf("Synthesized Polynomial: \n");
    poly.show();

    /////////////////// Evaluation with Oracle ///////////////////
    printf("\nEvaluation with Oracles...\n");

    // On accurate area
    double stable_origin_max_relative_err = 0;
    double stable_patch_max_relative_err = 0;

    double stable_origin_max_absolute_err = 0;
    double stable_patch_max_absolute_err = 0;

    auto inputs = randomOnValue(lbound, rbound, evaluationNum);
    for (int i = 0; i < evaluationNum; i++) {
        double xi = inputs[i];
        double origin_res = originFunc(xi);
        double patch_res = poly.evaluate(xi);
        mpreal oracle_res = oracleFunc(xi);

        // printf("=============\nx: %.3e\n", xi);
        // printf("origin: %.15e\n", origin_res);
        // printf("poly:   %.15e\n", patch_res);
        // printf("oracle: %.15e\n", (double)(oracle_res));;
        // getchar();

        double origin_absolute_err = (double)fabs(origin_res - oracle_res);
        double patch_absolute_err  = (double)fabs(patch_res - oracle_res);
        double origin_relative_err = (double)fabs((origin_res - oracle_res) / oracle_res);
        double patch_relative_err  = (double)fabs((patch_res - oracle_res) / oracle_res);

        stable_origin_max_relative_err = max(stable_origin_max_relative_err, origin_relative_err);
        stable_patch_max_relative_err  = max(stable_patch_max_relative_err, patch_relative_err);
        stable_origin_max_absolute_err = max(stable_origin_max_absolute_err, origin_absolute_err);
        stable_patch_max_absolute_err  = max(stable_patch_max_absolute_err, patch_absolute_err);
    }
    printf("Stable Area Improved By Naive Fitting (abs): %.2e -> %.2e\n", stable_origin_max_absolute_err, stable_patch_max_absolute_err);
    printf("Stable Area Improved By Naive Fitting (rel): %.2e -> %.2e\n", stable_origin_max_relative_err, stable_patch_max_relative_err);

    // On decayed area
    double decayed_origin_max_relative_err = 0;
    double decayed_patch_max_relative_err = 0;

    double decayed_origin_max_absolute_err = 0;
    double decayed_patch_max_absolute_err = 0;

    auto mags = randomOnMagnitude(-20, -2, evaluationNum);
    for (int i = 0; i < evaluationNum; i++) {
        double xi = (lbound + rbound)/2 + ((i%2==0) ? mags[i] : -mags[i]);
        double origin_res = originFunc(xi);
        double patch_res = poly.evaluate(xi);
        mpreal oracle_res = oracleFunc(xi);

        double origin_absolute_err = (double)fabs(origin_res - oracle_res);
        double patch_absolute_err  = (double)fabs(patch_res - oracle_res);
        double origin_relative_err = (double)fabs((origin_res - oracle_res) / oracle_res);
        double patch_relative_err  = (double)fabs((patch_res - oracle_res) / oracle_res);

        decayed_origin_max_relative_err = max(decayed_origin_max_relative_err, origin_relative_err);
        decayed_patch_max_relative_err  = max(decayed_patch_max_relative_err, patch_relative_err);
        decayed_origin_max_absolute_err = max(decayed_origin_max_absolute_err, origin_absolute_err);
        decayed_patch_max_absolute_err  = max(decayed_patch_max_absolute_err, patch_absolute_err);
    }
    printf("Decayed Area Improved By Naive Fitting (abs): %.2e -> %.2e\n", decayed_origin_max_absolute_err, decayed_patch_max_absolute_err);
    printf("Decayed Area Improved By Naive Fitting (rel): %.2e -> %.2e\n", decayed_origin_max_relative_err, decayed_patch_max_relative_err);

    printf("\nEvaluation on Function: %s Done.\n", functionIDs[funcIndex].c_str());

    printf("==============\n");

    ser.writeEvaluationToFile(
        decayed_origin_max_relative_err,
        decayed_patch_max_relative_err,
        decayed_origin_max_absolute_err,
        decayed_patch_max_absolute_err,
        stable_origin_max_relative_err,
        stable_patch_max_relative_err,
        stable_origin_max_absolute_err,
        stable_patch_max_absolute_err
    );
    return 0;
}

void testFunc(int funcIndex) {
    auto originFunc = simpleFuncList[funcIndex];
    auto oracleFunc = oracleFuncList[funcIndex];

    printf("Testing Function Number: %d\n", funcIndex);
    while (1) {
        double x;
        printf("Input >> ");
        scanf("%lf", &x);
        printf("origin: %.15e\n", originFunc(x));
        printf("oracle: %.15e\n", (double)oracleFunc(x));
    }
    return;
}

int matchFuncID(char* ID) {
    int singleindex=-1;
    for (int i = 0; i < simpleFuncList.size(); i++) {
        if (strcmp(ID, functionIDs[i].c_str()) == 0) {
            return i;
        }
    }
    return -1;
}

int main(int argc, char** argv) {

    Serializer ser;
    ser.cleanFuncIDFile();
    ser.cleanValidationFile();
    ser.cleanEvaluationFile();
    ser.cleanInfoLevelFile();

    if (argc > 1) {
        if (strcmp(argv[1],"noinfo")==0) {
            if (argc == 2) {
                ser.writeInfoLevelToFile(NoInfoLevel);
                for (int i = 0; i < simpleFuncList.size(); i++) {
                    // Useful in evaluation part.
                    fitFPResult(i, NO_WEIGHT); // Fitting with no information
                }
            }
            else {
                int funcIndex = matchFuncID(argv[2]);
                if (funcIndex == -1) {
                    printf("bin/testground.out noinfo [ functionID ]\n");
                    return 0;
                }
                else {
                    fitFPResult(funcIndex, NO_WEIGHT);
                }
            }
        }
        else if (strcmp(argv[1],"fullinfo")==0) {
            if (argc == 2) {
                ser.writeInfoLevelToFile(FullInfoLevel);
                for (int i = 0; i < simpleFuncList.size(); i++) {
                    // Useful in evaluation part.
                    fitFullInfo(i, USE_WEIGHT); // Fitting with full information from micro-structure
                }
            }
            else {
                int funcIndex = matchFuncID(argv[2]);
                if (funcIndex == -1) {
                    printf("bin/testground.out fullinfo [ functionID ]\n");
                    return 0;
                }
                else {
                    fitFullInfo(funcIndex, USE_WEIGHT);
                }
            }
        }
        else if (strcmp(argv[1],"onlyweight")==0) {
            ser.writeInfoLevelToFile(1);
            for (int i = 0; i < simpleFuncList.size(); i++) {
                // Useful in evaluation part.
                fitFPResult(i, USE_WEIGHT); // Fitting with full information from micro-structure
            }
        }
        else if (strcmp(argv[1],"runtime")==0) {
            for (int i = 0; i < simpleFuncList.size(); i++) {
                measurePatchRuntime(i, USE_WEIGHT);
            }
        }
        else {
            printf("bin/testground.out [ fullinfo / noinfo ]\n");
        }
        return 0;
    }

    // Default: Full info
    ser.writeInfoLevelToFile(FullInfoLevel);
    for (int i = 0; i < simpleFuncList.size(); i++) {
        // Useful in evaluation part.
        fitFullInfo(i, USE_WEIGHT); // Fitting with full information from micro-structure
    }

    // for (int i = 0; i < simpleFuncList.size(); i++) {
    //     // Unnecessary to provide figure, just words.
    //     // fitFPResult(i, USE_WEIGHT); // Fitting with FP, with weight
    //     // fitFullInfo(i, NO_WEIGHT);  // Fitting with mean, without weight
    //     // getchar();
    // }
    // fitFullInfo(7, USE_WEIGHT);

    return 0;
}
