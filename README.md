# Oracle-Free Repair Synthesis for Floating-Point Programs

## Setup

1. Install the [docker](https://www.docker.com/). For example:

    *  [docker for Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
    *  [docker for mac](https://docs.docker.com/docker-for-mac/install/)

2. Clone this repository to your local workspace:
```bash
$ git clone https://bitbucket.org/FP-Aceso/aceso.git
```

3. The workdir `aceso/` contains a `Dockerfile` for building the docker image.
```bash
$ cd aceso
$ docker build --network=host -t aceso .
```

    *  It may take a few minutes (~5 minutes) for installing necessary packages.

## Usage
Run the docker container with interactive mode:
```bash
$ docker run -it --network=host aceso /bin/bash
```

Now we are inside the docker container: `/aceso`. Run the following commands to generate all experiment's data:
```bash
# (we are in docker: /aceso)
$ make
$ bash run_write_patch.sh
```
The above commands will take around 3 minutes for all experiments, including

*  Synthesize patches
*  Validate patches
*  Evaluate patches with ground truth

The generated patches will be in `patches/patches.h`. Each patch is marked with `/* Valid Patch */` or `/* Invalid Patch */` at the beginning. (Only the last 4 control functions should be invalid).

### Reproduce Results
After the `python3 script/run.py`, we can reproduce the results easily.

*  **Reproduce Arrow Figures in Evaluation Part**
```
$ make
$ bin/testground.out && python3 script/draw-arrow-eval.py
$ bin/testground.out noinfo && python3 script/draw-arrow-eval.py
```

*  **Reproduce Micro-structure Figures**
```bash
$ python3 script/draw-micro-structure.py
```

The figures will be in the `figures/` folder. Next we will introduce how to copy figures to host machine.

### Retrieve Results to Host
We notice that you may need to view the `pdf` and `png` figures outside the docker container. The following commands should be helpful:

*  Escape / detach from docker container:
```
(in docker)
ctrl-p ctrl-q
```

*  Show docker container ID:
```
(in host)
$ docker container ls
CONTAINER ID        IMAGE        ...
<container_id>      aceso        ...
```

*   Copy files to host
```
(in host)
$ docker cp <container_id>:/aceso/figures .
$ docker cp <container_id>:/aceso/patches .
```

