FROM ubuntu:20.10
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y --no-install-recommends apt-utils && apt install -y --no-install-recommends \
  ca-certificates \
  git \
  build-essential \
  clang-11 \
  llvm-11 \
  python3 \
  python3-setuptools \
  python3-pip \
  libgsl-dev \
  libmpfr-dev \
  libmpfrc++-dev \
  libomp-dev \
  libalglib-dev \
  && pip3 install wheel mpmath numpy seaborn pandas matplotlib

RUN mkdir -p /aceso
COPY . /aceso
WORKDIR /aceso
